class ElevatorController:
	def __init__(self, elevator_quantity=3, floor_quantity=9):
		self.__floor_quantity = floor_quantity
		self.__elevators = [1 for item in range(elevator_quantity)]

	def call(self, called_floor):
		if (called_floor < self.__floor_quantity) and (called_floor > 0):
			comparator = lambda x: abs(x - called_floor)
			print(f'Elevators now at floors: {self.__elevators}')
			if self.__elevators.count(1) >= 2:
				floor = min(self.__elevators, key=comparator)
			else:
				floor = min([item for item in self.__elevators if item != 1], key=comparator)
			self.__elevators[self.__elevators.index(floor)] = called_floor
			print(f'Going from {floor} to {called_floor}')
		else:
			print('Wrong floor!')


if __name__ == '__main__':
	controller = ElevatorController()
	try:
		while True:
			f = int(input('Enter floor: '))
			controller.call(f)
	except Exception as e:
		print(e)
