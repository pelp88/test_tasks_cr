import random

array = [random.uniform(-1000, 1000) for item in range(25)]
print(f'Generated - {array}\n\nSorted - {list(sorted(array))}')
