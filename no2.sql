SELECT books.name, publishers.name
FROM books, publishers
WHERE (publishers.id = books.publisher_id)
AND (books.year > 2009)
AND (books.year < 2018)
AND (books.genre LIKE '%tech%')
AND (books.genre LIKE '%cyberpunk%');
